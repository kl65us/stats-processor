package types

import (
	"github.com/globalsign/mgo/bson"
	"time"
)

type SysWarnings struct {
	AlertType string `json:"alertType"`
	Channel   string `json:"channel"`
	Message   string `json:"message"`
	Topic     string `json:"topic"`
}

type ChannelBytes struct {
	Average    float32 `json:"average"`
	Count      int     `json:"count"`
	RampUpTime int     `json:"rampUpTime"`
	Total      int     `json:"total"`
	Variance   float32 `json:"variance"`
}

type ChannelConnection struct {
	LocalAddress  string `json:"localAddress"`
	RemoteAddress string `json:"remoteAddress"`
	Transport     string `json:"transport"`
}

type ChannelJitter struct {
	Average    float32 `json:"average"`
	Count      int     `json:"count"`
	RampUpTime int     `json:"rampUpTime"`
	Total      int     `json:"total"`
	Variance   float32 `json:"variance"`
}

type ChannelPackets struct {
	Average    float32 `json:"average"`
	Count      int     `json:"count"`
	RampUpTime int     `json:"rampUpTime"`
	Total      int     `json:"total"`
	Variance   float32 `json:"variance"`
}

type Channel map[string]struct {
	AudioCodec         string            `json:"audioCodec"`
	Bytes              ChannelBytes      `json:"bytes"`
	ChannelDisplayName string            `json:"channelDisplayName"`
	Connection         ChannelConnection `json:"connection"`
	Direction          string            `json:"direction"`
	Jitter             ChannelJitter     `json:"jitter"`
	Media              string            `json:"media"`
	Name               string            `json:"name"`
	PacketLoss         int               `json:"packetLoss"`
	Packets            ChannelPackets    `json:"packets"`
	Samples            int               `json:"samples"`
	StartTime          time.Time         `json:"startTime"`
	TotalBytes         int               `json:"totalBytes"`
	TotalPackets       int               `json:"totalPackets"`
}

type IterStat struct {
	ChannelReadyTime time.Time `json:"channelReadyTime"`
	Channels         Channel   `json:"channels"`
	Rank             float32   `json:"rank"`
	RankVersion      int       `json:"rankVersion"`
	SetupStartTime   time.Time `json:"setupStartTime"`
	VoiceDuration    int       `json:"voiceDuration"`
	VoiceStartTime   time.Time `json:"voiceStartTime"`
}

type TestIteration struct {
	BlobBaseName    string        `json:"blobBaseName"`
	Browser         string        `json:"browser"`
	ChartData       []byte        `json:"chartData"`
	CreateDate      time.Time     `json:"createDate"`
	DockerAgentId   string        `json:"dockerAgentId"`
	EndDate         time.Time     `json:"endDate"`
	Events          []interface{} `json:"events"`
	FirewallProfile string        `json:"firewallProfile"`
	InSessionIdx    int           `json:"inSessionIdx"`
	Location        string        `json:"location"`
	Machine         string        `json:"machine"`
	MediaId         string        `json:"mediaId"`
	MediaProfile    string        `json:"mediaProfile"`
	NetworkProfile  string        `json:"networkProfile"`
	Os              string        `json:"os"`
	Rank            int           `json:"rank"`
	RtcWarnings     []interface{} `json:"rtcWarnings"`
	RunIndex        int           `json:"runIndex"`
	RunName         string        `json:"runName"`
	SessionIdx      int           `json:"sessionIdx"`
	SessionSize     int           `json:"sessionSize"`
	StartDate       time.Time     `json:"startDate"`
	Stat            *IterStat     `json:"stat,omitempty"`
	Status          string        `json:"status"`
	SysWarnings     []SysWarnings `json:"sysWarnings"`
	TestDuration    int           `json:"testDuration"`
	TestRunId       bson.ObjectId `json:"testRunId"`
	Id              bson.ObjectId `json:"_id"`
}

// TODO: find what is in this metric
type CustomTestMetric struct {
}

type Overview struct {
	CallSetupTime []int    `json:"callSetupTime"`
	Rank          []int    `json:"rank"`
	Recv          struct { // TODO: not sure if here should be more keys for bits or something else
		BitRate    []float32 `json:"bitRate"`
		PacketLoss []float32 `json:"packetLoss"`
	} `json:"recv"`
	Send struct {
		Bytes        float32   `json:"bytes"`
		Channels     int       `json:"channels"`
		Duration     int       `json:"duration"`
		Jitter       float32   `json:"jitter"`
		PacketLoss   []float32 `json:"packetLoss"`
		RTT          int       `json:"rtt"`
		TotalBytes   int       `json:"totalBytes"`
		TotalPackets int       `json:"totalPackets"`
	} `json:"send"`
	Users []int `json:"users"`
}

// initially in Stat we have []byte (compressed by zlib)
// but once we decoded we need to parse into JSON using
// this struct format
type ChartData map[string]struct {
	Bits    []float32 `json:"bits"`
	Jitter  []int     `json:"jitter"`
	Loss    []int     `json:"loss"`
	Packets []float32 `json:"packets"`
	// commenting because inside object was 0, true which is not good
	// VideoBandwidthLimitedResolution []int     `json:"videoBandwidthLimitedResolution,omitempty"`
	// VideoCpuLimitedResolution       []int     `json:"videoCpuLimitedResolution,omitempty"`
	// VideoDelay                      []int     `json:"videoDelay,omitempty"` // not sure about int
	// VideoFrameRate                  []int     `json:"videoFrameRate,omitempty"`
}

type Stat struct {
	BrowserDuration  int              `json:"browserDuration"`
	CompleteCount    int              `json:"completeCount"`
	Count            int              `json:"count"`
	CustomTestMetric CustomTestMetric `json:"customTestMetric"`
	GroupEndTime     time.Time        `json:"groupEndTime"`
	GroupStartTime   time.Time        `json:"groupStartTime"`
	HasFailure       bool             `json:"hasFailure"`
	HasWarnings      bool             `json:"hasWarnings"`
	InProgress       []string         `json:"inProgress"` // not sure array of what types it should be
	Overview         Overview         `json:"overview"`
	Rank             float32          `json:"rank"`
	RankVersion      int              `json:rankVersion`
	Recv             struct {
		Bytes        float32 `json:"bytes"`
		Channels     int     `json:"channels"`
		Duration     int     `json:"duration"`
		Jitter       float32 `json:"jitter"`
		PacketLoss   int     `json:"packetLoss"`
		RTT          float32 `json:"rtt"`
		TotalBytes   int     `json:"totalBytes"`
		TotalPackets int     `json:"totalPackets"`
	} `json:"recv"`
	RtcWarnings []interface{} `json:"rtcWarnings"`
	Send        struct {
		Bytes        float32 `json:"bytes"`
		Channels     int     `json:"channels"`
		Duration     int     `json:"duration"`
		Jitter       float32 `json:"jitter"`
		PacketLoss   int     `json:"packetLoss"`
		RTT          float32 `json:"rtt"`
		TotalBytes   int     `json:"totalBytes"`
		TotalPackets int     `json:"totalPackets"`
	} `json:"send"`
	VoiceDuration  int     `json:"voiceDuration"`
	VoiceSetupTime float32 `json:"voiceSetupTime"`
}

type TestRun struct {
	TestId          bson.ObjectId `json:"testId"`
	MediaId         string        `json:"mediaId"`
	Project         bson.ObjectId `json:"project"`
	Manual          int           `json:"manual"`
	UserName        string        `json:"userName"`
	Name            string        `json:"name"`
	TestRunSort     string        `json:"test_run_sort"`
	StatusId        int           `json:"statusId"`
	MonitorName     string        `json:"monitorName"`
	MonitorId       string        `json:"monitorId"`
	MonitorNameSort string        `json:"monitor_name_sort"`
	RunName         string        `json:"runName"`
	RunMode         string        `json:"runMode"`
	RunOptions      string        `json:"runOptions"`
	Status          string        `json:"status"`
	PrevStatus      string        `json:"prev_status"`
	TextError       string        `json:"testError"`
	TextErrorLevel  string        `json:"testErrorLevel"`
	CreateDate      time.Time     `json:"createDate"`
	EndDate         time.Time     `json:"endDate"`
	RunDelay        int           `json:"runDelay"`
	TestScript      string        `json:"testScript"`
	Parameters      struct {
		IterationMode   string `json:"iterationMode"`
		LoopCount       int    `json:"loopCount"`
		Duration        int    `json:"duration"`
		ConcurrentUsers int    `json:"concurrentUsers"`
	} `json:"parameters"`
	Comments                 string      `json:"comments"`
	Flag                     bool        `json:"flag"`
	LogUrls                  interface{} `json:"logUrls"` // TODO: not sure what kind of type this is, need to find
	Shared                   bool        `json:"shared"`
	SystemInUse              string      `json:"systemInUser"`
	Env                      []string    `json:"env"`
	IterationComments        string      `json:"iterationComments"`
	IterationFlag            bool        `json:"iterationFlag"`
	IsRemote                 bool        `json:"isRemote"`
	EmailsToSendNotification []string    `json:"emailsToSendNotification"`
	UsageReport              interface{} `json:"usage_report"` // TODO: not sure about type here as well
}

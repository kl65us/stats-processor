package main

import (
	"bitbucket.org/kl65us/stats-processor/stats"
	"bitbucket.org/kl65us/stats-processor/types"
	. "bitbucket.org/kl65us/stats-processor/utils"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

func Greet(w http.ResponseWriter, r *http.Request) {
	Print("Greet function")
	fmt.Fprintf(w, "Hello there")
}

func LogJson(w http.ResponseWriter, r *http.Request) {
	Warn("LogJson beginning")

	// for now only if PartialLoad is false
	type InputData struct {
		Iterations  []types.TestIteration `json:"iterations"`
		TestRun     types.TestRun         `json:"testRun"`
		Stat        types.Stat            `json:"stat"`
		PartialLoad bool                  `json:"partialLoad"`
	}

	Print("Taking raw json from body")
	var inputObject InputData
	content, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(err)
	}

	Print("Unmarshaling json")
	err = json.Unmarshal(content, &inputObject)
	if err != nil {
		fmt.Println(err)
	}

	Print("Looking for prepared iterations (where iter.stat exists)")
	preparedIters := make([]*types.TestIteration, 0)
	fmt.Println(preparedIters)
	for k, iter := range inputObject.Iterations {
		Print(fmt.Sprint(strconv.Itoa(k)))
		if iter.Stat != nil {
			Print("Appending iteration")
			// fmt.Printf("%+v\n", preparedIters)
			preparedIters = append(preparedIters, &inputObject.Iterations[k])
		} else {
			fmt.Println("No Iter.Stat")
		}
	}

	// calc getAccomodatedStats so you will have chartsData
	st := stats.GetAccomodatedStats(inputObject.TestRun, preparedIters, inputObject.Stat)
	fmt.Println(st)
}

func main() {
	port := ":3200"
	Print("Stats calculator service (is running) at port " + port)
	// for simple test
	http.HandleFunc("/hello", Greet)
	// take a json from main backend and log it out
	http.HandleFunc("/parse", LogJson)

	http.ListenAndServe(port, nil)
}

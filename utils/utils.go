package utils

import (
	"fmt"
	"github.com/fatih/color"
	"strings"
)

var log = color.New(color.FgGreen).Add(color.Bold)
var logWarn = color.New(color.FgHiYellow).Add(color.Bold)

// TODO: make an object and reuse code
func Print(comment string) {
	wrapper := strings.Repeat("=", 12)
	output := fmt.Sprintf("%s %s %s\n", wrapper, comment, wrapper)
	log.Print(output)
}

func Warn(comment string) {
	wrapper := strings.Repeat("=", 12)
	output := fmt.Sprintf("%s %s %s\n", wrapper, comment, wrapper)
	logWarn.Print(output)
}

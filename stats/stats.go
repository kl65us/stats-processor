package stats

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"reflect"
	"strconv"

	"bytes"
	"compress/gzip"

	"bitbucket.org/kl65us/stats-processor/types"
	. "bitbucket.org/kl65us/stats-processor/utils"
)

type AccomodatedStat struct {
	Some string
}

// export async function getAccomodatedStats(testRun: any, testIterations: any[], stat: any): Promise<any> {
func GetAccomodatedStats(testRun types.TestRun, testIterations []*types.TestIteration, stat types.Stat) AccomodatedStat {
	Warn("GetAccomodatedStats")
	// final output, not sure about type structure at the moment

	// fmt.Println(testIterations[2].ChartData)
	if !reflect.DeepEqual(stat, (types.Stat{})) {
		iterLen := len(testIterations)
		charts := make(<-chan *types.ChartData, iterLen)
		charts = uncompress(testIterations)

		for i := 0; i < iterLen; i++ {
			<-charts

			// TODO: take data in parallel, calculate it and form another struct

		}
	} else {
		// for debug purpose just tell that we have no stat
	}

	return AccomodatedStat{}
}

// uncompress uncompresses chart data and returns json
func uncompress(iterations []*types.TestIteration) <-chan *types.ChartData {
	uncompressed := make(chan *types.ChartData, len(iterations))
	for k, iter := range iterations {
		go func(n int, iter *types.TestIteration, ch chan *types.ChartData) {
			Warn("Doin" + strconv.Itoa(n))
			var uncomp types.ChartData
			compressed := iter.ChartData

			gr, err := gzip.NewReader(bytes.NewBuffer(compressed))
			defer gr.Close()
			if err != nil {
				fmt.Println(err)
			}

			data, err := ioutil.ReadAll(gr)
			if err != nil {
				fmt.Println(err)
			}

			err = json.Unmarshal(data, &uncomp)
			if err != nil {
				fmt.Println(err)
			}

			uncompressed <- &uncomp
		}(k, iter, uncompressed)
	}

	return uncompressed
}
